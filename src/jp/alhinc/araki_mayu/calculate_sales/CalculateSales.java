package jp.alhinc.araki_mayu.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;

public class CalculateSales {
	public static void main(String[] args) {

		HashMap<String, String> branchMap = new HashMap<>();
		HashMap<String, Long> salesMap = new HashMap<>();
		BufferedReader br = null;

		try {
			File file = new File(args[0], "branch.lst");
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			br = new BufferedReader(new FileReader(file));
			String line;
			while ((line = br.readLine()) != null) {
				String[] shop = line.split(",");
				if (!shop[0].matches("^[0-9]{3}$") || shop.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				String code = new String(shop[0]);
				String name = new String(shop[1]);
				branchMap.put(code, name);
				salesMap.put(code, 0L);
			}
			File dir = new File(args[0]);
			File files[] = dir.listFiles();
			Arrays.sort(files);
			int preNumber = -1;
			for (int i = 0; i < files.length; i ++) {
				if (files[i].getName().matches("^[0-9]{8}\\.rcd$") && files[i].isFile()) {
					int fileNumber = Integer.parseInt(files[i].getName().substring(0, 8));
					if (preNumber != -1 && fileNumber - preNumber != 1) {
						System.out.println("売上ファイル名が連番になっていません");
						return;
					}
					br = new BufferedReader(new FileReader(files[i]));
					String code = br.readLine();
					String price = br.readLine();
					if (br.readLine() != null) {
						System.out.println(files[i].getName() + "のフォーマットが不正です");
						return;
					}
					long prices = Long.parseLong(price);
					if (salesMap.containsKey(code)) {
						long sum = salesMap.get(code) + prices;
						String overSum = String.valueOf(sum);
						if (overSum.length() > 10) {
							System.out.println("合計金額が10桁を超えました");
							return;
						}
						salesMap.put(code, sum);
					} else {
						System.out.println(files[i].getName() + "の支店コードが不正です");
						return;
					}
					preNumber = Integer.parseInt(files[i].getName().substring(0, 8));
				}
			}
			File txt = new File(args[0], "branch.out");
			FileWriter fw = new FileWriter(txt);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
			for (String key : branchMap.keySet()) {
				pw.println(key + "," + branchMap.get(key) + "," + salesMap.get(key));
			}
			pw.close();
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
	}
}